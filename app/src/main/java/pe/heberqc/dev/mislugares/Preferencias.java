package pe.heberqc.dev.mislugares;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by heber_000 on 31/05/2015.
 */
public class Preferencias extends PreferenceActivity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }

}
