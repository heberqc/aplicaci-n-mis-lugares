package pe.heberqc.dev.mislugares;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;

public class AcercaDe extends Activity {

    MediaPlayer mp;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acercade);
        mp = MediaPlayer.create(this, R.raw.audio);
        mp.start();

    }

    @Override
    protected void onSaveInstanceState(Bundle estadoGuardado){
        super.onSaveInstanceState(estadoGuardado);
        if (mp != null) {
            int pos = mp.getCurrentPosition();
            estadoGuardado.putInt("posicion", pos);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle estadoGuardado){
        super.onRestoreInstanceState(estadoGuardado);
        if (estadoGuardado != null && mp != null) {
            int pos = estadoGuardado.getInt("posicion");
            mp.seekTo(pos);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mp.pause();
    }
}